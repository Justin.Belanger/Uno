//********************************
//Nom: Justin Belanger
//description : fonctionalitee du jeux uno, cree et jouer des cartes
//nom du programme: uno
//********************************

//on click to draw a new card
document.getElementById("New-card").onclick = function() { 
    if(usersTurn){
        //give new card to the player
        showContent(newCard());
        pickCardSound.play();

        //take up the users turn 
        usersTurn = false;
        playCpuCardAfterDelay();    
    }
};

let pickCardSound = document.getElementById("pickCardSound");
let dealSound = document.getElementById("dealSound");

let deck = document.getElementById("deck");
let handDiv = document.getElementById("hand");

let usersTurn = true;

//card on top of the deck (never include cards with extra functions to the start)
let cardOnTop = {color : getRandColor(), number : Math.floor(Math.random() * 11)}
setFirstCard(cardOnTop);

function setFirstCard(cardOnTop){
    let temp, item, a, txt;

    //get the template for new card
    temp =  document.getElementsByTagName("template")[0];
    item = temp.content.querySelector("div");

    //put the template into the a usable element varriable
    a = document.importNode(item, true);

    //set the class name to set the color
    a.className = cardOnTop.color;

    //get the p element and set its text to the right number
    txt = a.getElementsByTagName("p")[0];
    txt.textContent = cardOnTop.number;  

    //empty the deck and append the new card
    deck.innerHTML ="";
    deck.appendChild(a);
}

//holds the card objects in the players hand
let cpuHand=[];

//get the new cards to start the game
for(let i =0; i< 8; i++){
    //add the object to the player's hand
    showContent(newCard());

    cpuHand.push(newCard());
    cpuPickCard(cpuHand[i]);
}

//create a new random card object
function newCard(){
    let card = {color : getRandColor(), number : getRandNumber(), element : null}
    return card;
}

//give a specific card to the players hand
function showContent(card){
    let temp, item, a, txt;

    //get the template for new card
    temp =  document.getElementsByTagName("template")[0];
    item = temp.content.querySelector("div");

    //put the template into the a varriable
    a = document.importNode(item, true);

    //if the card symbol is one of a black card, ignore the current color and chang eit to black
    if(card.number === "+4" || card.number === "?"){
        card.color = "carte noir"
    }
    a.className = card.color;
    
    txt = a.getElementsByTagName("p")[0];
    if(card.number === "skip"){
        //add the image inside the card
        let img = document.createElement("img");
        img.src="./img/skip.png";

        a.innerHTML="";
        a.appendChild(img)

    }
    else{
        //add the text element inside the card    
        txt.textContent = card.number;  
    }
    
    //append the card element and link the element to the card object
    handDiv.appendChild(a);
    card.element = a;

    //reset the fanning of the cards
    fanCards();
    
    //the card on click
    a.onclick = function() {
        //only play the card if color or number matches, or the card is black
        if((cardOnTop.color === card.color || cardOnTop.number === card.number || card.color === "carte noir")&& usersTurn &&  a.parentNode != deck ){

            usersTurn =false;

            //change the current card values
            cardOnTop.color = card.color;
            cardOnTop.number = card.number;

            //empty the deck and put the new card in its place
            deck.innerHTML ="";
            deck.appendChild(a);

            //if the user has used all of his cards, call the userwins function
            if(handDiv.childElementCount === 0){
                userWins();
            }

            //reset the card rotation
            a.style.transform = "rotate(0deg)"

            //if the card is black/can change the current color
            if(card.color === "carte noir"){
                //get the colorpicker form its template
                let colorPickerTemp = document.getElementsByTagName("template")[1];
                let colorPickerItem = colorPickerTemp.content.querySelector("div");

                //apply the template
                let colorPicker = document.importNode(colorPickerItem, true);
                deck.appendChild(colorPicker)

                //set the OnClick to set the current color
                document.getElementById("pick-rouge").onclick = function(){
                    setColor("carte rouge", a)
                }
                document.getElementById("pick-bleu").onclick = function(){
                    setColor("carte bleu", a)
                }
                document.getElementById("pick-jaune").onclick = function(){
                    setColor("carte jaune", a)
                }
                document.getElementById("pick-vert").onclick = function(){
                    setColor("carte vert", a)
                }

                //if the card was a pick 4, the cpu picks 4
                if(card.number === "+4"){
                    cpuPicksCards(4);
                }
            }
            //if the card is a skip, reset the turn to him
            else if(card.number === "skip"){
                playerSkips();
            }
            else{
                //if the card is +2, cpu picks 2
                if(card.number==="+2"){
                    cpuPicksCards(2);
                }

                //set the turn to the cpu after a short delay
                playCpuCardAfterDelay();
            }
            
            //play a sound and reset the card fan
            pickCardSound.play();
            fanCards();
        }
    }

    //make the card gibber and higher if the mouse is over it
    a.onmouseover = function(){
        //only if the card is still in the players deck
        if(a.parentNode != deck)
        setCardTransform(a,true,0);
    }
    a.onmouseout = function(){
        setCardTransform(a,false,0);
        fanCards();
    }
}

//set the color of a black card that has been played
function setColor(color, blackCard){
    //set the color of the black card
    blackCard.className = color;
    cardOnTop.color = color;

    //empty the deck and put the card back in it (in order to remove the color picker)
    deck.innerHTML="";
    deck.appendChild(blackCard);

    //the cpu only picks a card after the button is pressed
    playCpuCardAfterDelay();
}

//return a random number / symbol for the card to hold
function getRandNumber(){
    let num = Math.floor(Math.random() * 15);

    //change the return for each special card
    switch(num){
        case 11:
            num = "skip"
            break;
        case 12:
            num = "+2"
            break;
        case 13:
            num = "+4"
            break;
        case 14:
            num = "?"
            break;

    } 
    return num;
}

//return the name of the class to change the color of a card
function getRandColor(){
    let col = Math.floor(Math.random()*4 +1);

    if(col===1){
        //blue
        return "carte bleu"
    }
    else if(col ===2){
        //red
        return "carte rouge"
    }
    else if(col ===3){
        //green
        return "carte vert"
    }
    else if(col ===4){
        //yellow
        return "carte jaune"
    }
}

//rotate every card in the user and cpu hands in a way normal cards are held
function fanCards(){
    //dont fan cards when on moblie
    if(window.innerWidth > 700){
        let handElements = document.getElementById("hand").querySelectorAll("div");

        //total degrees of the fan
        let degrees =50;

        //get the degree interval for each card 
        let interval = degrees / (handElements.length +1);

        //the degree to start the fan 
        let rotateBy = -(degrees/2);

        for(let i =0; i<handElements.length; i++){
            //add the interval to the current rotation
            rotateBy = rotateBy+ interval;
            setCardTransform(handElements[i], false, rotateBy)
        }

        //fan the Cpu cards in the same way
        let cpuHandElements = document.getElementById("hand-cpu").querySelectorAll("div");
        interval = degrees / (cpuHandElements.length +1);
        rotateBy = degrees/2;

        for(let i =0; i<cpuHandElements.length ; i++){
            //remove the interval to the current rotation because they are reversed 180 degrees
            rotateBy = rotateBy- interval;
            setCardTransform(cpuHandElements[i], false, rotateBy)
        } 
    }
}

//set the transform css to a selected card, if selected is true -> the item gets bigger and higher for a better view
function setCardTransform(element, isSelected, rotation){
    if(isSelected){
        //if the item is selected, change its scale, translation and no rotation 
        element.style.transform = "rotate("+rotation+") scale(1.1, 1.1) translate(0em, -1em)";
        element.style.zIndex = "1";
    }
    else{
        //only set the rotation
        element.style.transform = "rotate("+rotation+"deg)";
        element.style.zIndex = "0";
    }
}

//section for the cpu
//add the back of a card  element to the cpu hand array and div
function cpuPickCard(card){
    let temp, item, a;

    //get the template for new card
    temp =  document.getElementsByTagName("template")[2];
    item = temp.content.querySelector("div");

    //put the template into the a varriable
    a = document.importNode(item, true);
    let hand = document.getElementById("hand-cpu");

    hand.appendChild(a);
    card.element = a;
    fanCards();
}

//the cpu checks every card one by one until one can be played
function cpuPlayCard(){
    //if cpu doesnt have a card that can be played, he draws one   
    let cpuHasCard = false;

    for(let i =0; i<cpuHand.length;i++){
        //if the card can be played
        if(cpuHand[i].color === cardOnTop.color || cpuHand[i].number === cardOnTop.number || cpuHand[i].color ==="carte noir"){
            //play the sound

            pickCardSound.play();
            //set the boolean
            cpuHasCard = true;

            let temp, item, a, txt;

            //get the template for new card
            temp =  document.getElementsByTagName("template")[0];
            item = temp.content.querySelector("div");

            //put the template into the a varriable
            a = document.importNode(item, true);

            //if the card symbol is one of a black card, ignore the current color and change it to a random one
            if(cpuHand[i].number === "+4" || cpuHand[i].number === "?"){
                cpuHand[i].color = getRandColor();
            }

            //set the class name and number to change the apperance
            a.className = cpuHand[i].color;
            txt = a.getElementsByTagName("p")[0];

            //if the card has a draw function, the player draws
            if(cpuHand[i].number === "+4"){
                playerPicksCards(4);
            }
            else if(cpuHand[i].number === "+2"){
                playerPicksCards(2);
            }

            //set the values for the card on top
            cardOnTop.color = cpuHand[i].color;
            cardOnTop.number = cpuHand[i].number;
            
            //if the card is a skip, set the image and cpu will play again
            let replay = false;
            if(cpuHand[i].number === "skip"){
                //add the image inside the card
                let img = document.createElement("img");
                img.src="./img/skip.png";

                //empty the card and replace it with the skip image
                a.innerHTML="";
                a.appendChild(img)

                //sets the turn to the cpu again
                cpuSkips();

                //remove the card from the cpu deck
                cpuHand[i].element.remove();
                cpuHand.splice(i,1);

                //cpu will play again
                replay = true;
            }
            else{
                //if the card is normal/not a skip

                //add the text element inside the card    
                txt.textContent = cpuHand[i].number;  
                //remove the card from the cpu deck
                cpuHand[i].element.remove();
                cpuHand.splice(i,1);
            }

            //append the card element and link the element to the card object
            deck.innerHTML = "";
            deck.appendChild(a);

            //reset the fan
            fanCards();

            //if the cpu has no more cards, he wins
            if(cpuHand.length === 0){
                cpuWins();
            }

            //if it was a skip card, he plays again
            if(replay){
                playCpuCardAfterDelay();
            }
            break;
        }
    }

    //if he couldnt play a card, pick one
    if(!cpuHasCard){
        cpuHand.push(newCard());
        pickCardSound.play();
        cpuPickCard(cpuHand[cpuHand.length -1]);
    }
    
    //set the turn to the user
    usersTurn =true;
    toggleGrayScale();
}

//fonctions for special cards

//skips
//skip the cpu's turn
function playerSkips(){
    usersTurn = true;
}

//skip the user's turn
function cpuSkips(){
    usersTurn = false;
}

//picking multiple cards +2 and +4 cards
//pick new cards and add them to the user's deck
function playerPicksCards(amount){
    dealSound.play();
    for(let i = 0; i< amount ; i++){
        showContent(newCard());
    }
}

//pick new cards and add them to the cpu's deck
function cpuPicksCards(amount){
    dealSound.play();
    for(let i = 0; i< amount ; i++){
        cpuHand.push(newCard());
        cpuPickCard(cpuHand[cpuHand.length -1]);
    }
}

//plays the function where the cpu plays a card but after a random delay
function playCpuCardAfterDelay(){
    toggleGrayScale();
    usersTurn = false;
    let delay = Math.random()*2 +1;
    window.setTimeout(cpuPlayCard, delay *1000);
}

//apply a green tint to the screen a say the user won
//get a template holding the you win screen and show it over the game
function userWins(){
    //get the div from the template
    temp =  document.getElementsByTagName("template")[3];
    item = temp.content.querySelector("div");

    //put the template into the a usable element
    a = document.importNode(item, true);
    document.getElementsByClassName("table")[0].appendChild(a);
}

//apply a red tint ans say the user lost
//get a template holding the scrren a show it
function cpuWins(){
     //get the div from the template
     temp =  document.getElementsByTagName("template")[4];
     item = temp.content.querySelector("div");
 
     //put the template into the a usable element
     a = document.importNode(item, true);
     document.getElementsByClassName("table")[0].appendChild(a);
}

//set a gray scale to the hand if it is not the user's turn
let grayOn = false;
function toggleGrayScale(){
    grayOn = !(grayOn);

    if(grayOn){
        handDiv.style.filter = "grayscale(100%)";
    }
    else{
        handDiv.style.filter = "grayscale(0%)";
    }
}